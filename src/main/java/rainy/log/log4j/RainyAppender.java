package rainy.log.log4j;

import org.apache.log4j.AsyncAppender;
import org.apache.log4j.spi.LoggingEvent;

/**
 * 
 * 该类将会写入到本地和远程
 * 
 * @author dongwenbin
 *
 */
public class RainyAppender extends AsyncAppender {

	private String local;

	public void append(LoggingEvent event) {
		super.append(event);
		String message = this.layout.format(event);
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

}