package rainy.file.synchronization.server;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import rainy.file.synchronization.config.RainyRunTime;

/**
 * 
 * @author Grom
 *
 */
public class MonitorServer {

	private static int port = RainyRunTime.getIntegerPropDefine("rainy.master.port", 9054);

	/**
	 * use the default port to do the sync
	 */
	public MonitorServer() {

	}

	public MonitorServer(int port) {
		this.port = port;
	}

	public void run() throws Exception {
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
			ServerBootstrap serverBootstrap = new ServerBootstrap();
			serverBootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
					.childHandler(new MonitorServerInitializer()).option(ChannelOption.SO_BACKLOG, 128)
					.childOption(ChannelOption.SO_KEEPALIVE, true);
			System.out.println("[server] server is started at [" + this.port + "]");
			// 绑定端口，开始接收进来的连接
			ChannelFuture f = serverBootstrap.bind(port).sync();
			// 等待服务器 socket 关闭 。
			// 在这个例子中，这不会发生，但你可以优雅地关闭你的服务器。
			f.channel().closeFuture().sync();
		} finally {
			workerGroup.shutdownGracefully();
			bossGroup.shutdownGracefully();

			System.out.println("[server] server 关闭了");
		}
	}

	public static void main(String[] args) throws Exception {
		new MonitorServer(port).run();
	}
}