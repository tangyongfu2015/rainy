package rainy.file.synchronization.log;

import rainy.file.synchronization.config.RainyRunTime;

/**
 * Please do not use this logger in your code<br>
 * Rainy use this only
 * 
 * @author dongwenbin
 *
 */
public class RainnyLogger {
	private static Object output;
	/**
	 * 0=debug,1=info
	 */
	private static int loglevel = 0;
	private static String logLevelConfig = RainyRunTime.getStringPropDefine("rainy.logger.level", "INFO");

	static {
		if (logLevelConfig != null && logLevelConfig.equalsIgnoreCase("INFO")) {
			loglevel = 1;
		}
	}

	public static void info(String message) {
		if (loglevel <= 1) {
			System.out.println(message);
		}
	}

	public static void debug(String message) {
		if (loglevel <= 0) {
			System.out.println(message);
		}
	}
}
