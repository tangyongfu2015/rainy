package rainy.file.synchronization.client;

import java.io.File;
import java.io.FileInputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import rainy.file.synchronization.client.channel.ChannelFactory;
import rainy.file.synchronization.client.file.FolderMonitor;
import rainy.file.synchronization.config.RainyRunTime;

/**
 * 
 * @author Grom
 *
 */
public class FileSendClient {

	private String host = RainyRunTime.getStringPropDefine("rainy.master.serverName", "localhost");
	private int port = RainyRunTime.getIntegerPropDefine("rainy.master.port", 9054);
	public static String monitorFolder = RainyRunTime.monitorFolder;
	private static BlockingQueue<File> modifiedFilesQueue = new LinkedBlockingDeque<>();
	private static final Object key = new Object();
	private static final String clientType = "fileSender";

	public FileSendClient() {

	}

	public FileSendClient(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public static void addNewAddedFile(File file) {
		synchronized (key) {
			modifiedFilesQueue.add(file);
		}
	}

	public void run() throws Exception {
		EventLoopGroup group = new NioEventLoopGroup();
		try {
			// init channel while start!
			Channel channel = ChannelFactory.getCurrentChannel(clientType, group);
			System.out.println("[ client ] Client is started to connect [" + host + ":" + port + "]");
			new FolderMonitor(monitorFolder).start();
			System.out.println("[ client ] Monitor is started!");
			boolean exit = false;
			while (true && !exit) {
				File newFile = null;
				newFile = modifiedFilesQueue.take();
				System.out.println("Start to sync file : " + newFile);
				if (newFile == null) {
					Thread.sleep(1000);
					continue;
				}
				String sendStr = newFile.getPath().substring(monitorFolder.length()) + "#";
				// 发送该文件
				while (true) {
					Thread.sleep(100);
					if (newFile.getParentFile().exists() && newFile.exists() && newFile.canWrite()) {
						FileInputStream fins = new FileInputStream(newFile);
						byte[] buffer = new byte[fins.available()];
						fins.read(buffer);
						fins.close();
						byte[] b = new org.apache.commons.codec.binary.Base64().encode(buffer);
						sendStr += new String(b);
						channel = ChannelFactory.getCurrentChannel(clientType, group);
						channel.writeAndFlush(sendStr + "\r\n");
						break;
					}
				}
			}
		} finally {
			group.shutdownGracefully();
		}
	}

	public static void main(String[] args) throws Exception {
		new FileSendClient().run();
	}
}