package rainy.file.synchronization.client.file.traverse;

import java.io.File;

import rainy.file.synchronization.config.RainyRunTime;

/**
 * 将本地文件序列化為一棵树的方法<br>
 * 如果文件比较大，则取文件的头部的100个byte跟末尾的100个byte，并进行hash运算<br>
 * 服务器端进行hash比对，如果一致，则任务是同一个文件。
 * 
 * @author dongwenbin
 *
 */
public class LocalFileTraverser {
	public static FileStructure generateFileStructure(File file, FileStructure structure) {
		System.out.println(file);
		if (file.isFile()) {
			FileStructure _s = new FileStructure();
			_s.setFile(true);
			_s.setFileName(file.getName());
			_s.setFilePath(file.getPath().substring(RainyRunTime.monitorFolder.length()));
			_s.setHashCode(FileHasher.generateFileHash(file));
			if (structure != null) {
				structure.getChildren().add(_s);
			} else {
				return _s;
			}
		} else {
			if (structure == null) {
				structure = new FileStructure();
				structure.setFile(true);
				structure.setFileName(file.getName());
				structure.setFilePath(file.getPath().substring(RainyRunTime.monitorFolder.length()));
			}
			File[] files = file.listFiles();
			for (File _f : files) {
				generateFileStructure(_f, structure);
			}
		}
		return structure;
	}

	public static void main(String[] args) {
		System.out.println(generateFileStructure(new File("d:/logs"), null));
	}
}
